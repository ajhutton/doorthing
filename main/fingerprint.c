#include "driver/uart.h"
#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "fingerprint.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

static const char *FPTAG = "FINGERPRINT";

#define BUF_SIZE (1024)
#define EX_UART_NUM UART_NUM_2
extern bool finger,tzMode;

extern void servoAction(char, int);
extern void open_sound();
extern void openSesame();


char headerBytes[6] = {0xEF, 0x01, 0xff, 0xff, 0xff, 0xff}; //address hardcoded as ffffff who cares

void sendData(char commandData[], int commandLength) {
    char packetType = FINGERPRINT_COMMANDPACKET; //TODO: make this an argument
    char *data = (char *) malloc(BUF_SIZE); //create send buffer
    memcpy(data, headerBytes, 6); //copy headerBytes EF01FFFFFF into start of buffer - this remains unchanged
    data[6] = packetType; //set field for packet type ()

    int wire_length = commandLength +2; //packet size = length + checksum bytes
    //ESP_LOGI(FPTAG,"------wire length is %i \n", wire_length);

    data[7] = 0x00; //combine into one?
    data[8] = wire_length;


    unsigned short sum = ((wire_length)>>8) + ((wire_length)&0xFF) + packetType;
    for (int i=0; i< commandLength; i++) {
      sum += commandData[i];
        data[9+i] = commandData[i]; //something in here 
      }
  data[(9 + commandLength)] = 0x00; //prependchecksum with 00 
  data[(9 + commandLength + 1)] = sum;

  uart_write_bytes(EX_UART_NUM, (const char*) data, 11 + commandLength);
  free(data);
  data = NULL;
        //vTaskDelete(NULL);
}


void processPacket(uint8_t* dtmp) {
   
/*      for(int i=0; i < 10; i++) {
          printf("%02x ",*(dtmp + i) & 0xff);
      }
      */
   switch(dtmp[9]) {
    case FINGERPRINT_OK: 
    if(!finger) {
      ESP_LOGI(FPTAG,"finger detected\n");
      finger = true;
      sendData((char[]){FINGERPRINT_IMAGE2TZ, 0x01}, 2);
    } else if (tzMode) 
    {
                                 // pubMQTT(printf("{user: %02x, confidence: %02x",dtmp[11],dtmp[13]));
      ESP_LOGI(FPTAG, "match detected %02x with score %02x \n", dtmp[11], dtmp[13]);
      open_sound();
      openSesame();
      
      finger = false;
      tzMode = false;
    } else {
      tzMode = true;
      ESP_LOGI(FPTAG, "character file generated\n");
                                      //sendData((char[]){FINGERPRINT_SEARCH, 0x01, 0x00, 0x03}, 2);
      sendData((char[]){FINGERPRINT_WEIRD, 0x01, 0x00, 0x00, 0x00, 0x97}, 6);
    }
    break;
    case FINGERPRINT_NOTFOUND:
    printf("fingerprint not found in db\n");
    finger = false;
    tzMode = false;
    break;
  }
}