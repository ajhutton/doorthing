#define SERVO_PIN 32
#define PWM_SERVO_LEFT  1000
#define PWM_SERVO_IDLE  1500
#define PWM_SERVO_RIGHT 2000

#define PWMTAG "PWM"

#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "nvs_flash.h"


void pwm_servo_timed(struct servo_cmd *cmd) {
    mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_LEFT);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_IDLE);
}

void servoAction(char dir, int time) { //0 left 1 right
	if(dir == 'L') {
		mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_LEFT);
		vTaskDelay(time / portTICK_PERIOD_MS);
	    mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_IDLE);
	} else {
		mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_RIGHT);
		vTaskDelay(time / portTICK_PERIOD_MS);
	    mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_IDLE);
	}
}
void openSesame() {
      servoAction('L', 700);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      servoAction('R', 700);
}

void servo_init() {
	ESP_LOGI(PWMTAG, "PWM configuration");
    mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, SERVO_PIN);    //Set GPIO 18 as PWM0A, to which servo is connected
    mcpwm_config_t pwm_config;
    pwm_config.frequency = 50;    //frequency = 50Hz, i.e. for every servo motor time period should be 20ms
    pwm_config.cmpr_a = 0;    //duty cycle of PWMxA = 0
    pwm_config.cmpr_b = 0;    //duty cycle of PWMxb = 0
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);    //Configure PWM0A & PWM0B with above settings
    mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, PWM_SERVO_IDLE); //Idle

}