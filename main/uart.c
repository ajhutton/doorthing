#include "driver/uart.h"
#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "fingerprint.c"



static const char *UARTAG = "uart_events";
#define TXD_PIN 17
#define RXD_PIN 16


#define BUF_SIZE (1024)

#define RD_BUF_SIZE (BUF_SIZE)
#define EX_UART_NUM UART_NUM_2
#define PATTERN_CHR_NUM    (3)

static QueueHandle_t uart2_queue;


//extern int pubMQTT(char[] );



void waitForFinger(void *pvParameter) {
    while(1)
    {
        if(!finger) {
            //ESP_LOGI(FPTAG,"asking...");

         sendData((char[]){FINGERPRINT_GETIMAGE}, 1);
     }
     vTaskDelay(200 / portTICK_RATE_MS);
 }
}

static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    size_t buffered_size;
    uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
    for(;;) {
        //Waiting for UART event.
        if(xQueueReceive(uart2_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
            bzero(dtmp, RD_BUF_SIZE);
            //ESP_LOGI(UARTAG, "uart[%d] event:", EX_UART_NUM);
            switch(event.type) {
                //Event of UART receving data
                /*We'd better handler data event fast, there would be much more data events than
                other types of events. If we take too much time on data event, the queue might
                be full.*/
                case UART_DATA:
                uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);
                /*for(int i=0; i < event.size; i++) {
                   printf("%02x ",dtmp[i] & 0xff);

               }
               printf("\n");
*/
                    if (dtmp[6] == FINGERPRINT_ACKPACKET) { //then this is an ACK package
                        processPacket(dtmp);

                    }

                    break;
                //Event of HW FIFO overflow detected
                    case UART_FIFO_OVF:
                    ESP_LOGI(UARTAG, "hw fifo overflow");
                    // If fifo overflow happened, you should consider adding flow control for your application.
                    // The ISR has already reset the rx FIFO,
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart2_queue);
                    break;
                //Event of UART ring buffer full
                    case UART_BUFFER_FULL:
                    ESP_LOGI(UARTAG, "ring buffer full");
                    // If buffer full happened, you should consider encreasing your buffer size
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart2_queue);
                    break;
                //Event of UART RX break detected
                    case UART_BREAK:
                    ESP_LOGI(UARTAG, "uart rx break");
                    break;
                //Event of UART parity check error
                    case UART_PARITY_ERR:
                    ESP_LOGI(UARTAG, "uart parity error");
                    break;
                //Event of UART frame error
                    case UART_FRAME_ERR:
                    ESP_LOGI(UARTAG, "uart frame error");
                    break;
                //UART_PATTERN_DET
                    case UART_PATTERN_DET:
                    uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
                    int pos = uart_pattern_pop_pos(EX_UART_NUM);
                    ESP_LOGI(UARTAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                    if (pos == -1) {
                        // There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
                        // record the position. We should set a larger queue size.
                        // As an example, we directly flush the rx buffer here.
                        uart_flush_input(EX_UART_NUM);
                    } else {
                        uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
                        uint8_t pat[PATTERN_CHR_NUM + 1];
                        memset(pat, 0, sizeof(pat));
                        uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
                        ESP_LOGI(UARTAG, "read data: %s", dtmp);
                        ESP_LOGI(UARTAG, "read pat : %s", pat);
                    }
                    break;
                //Others
                    default:
                    ESP_LOGI(UARTAG, "uart event type: %d", event.type);
                    break;
                }
            }
        }
        free(dtmp);
        dtmp = NULL;
        vTaskDelete(NULL);
    }

    void initFP() {
        const uart_config_t uart_config = {
            .baud_rate = 57600,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
        };
        uart_param_config(UART_NUM_2, &uart_config);
        esp_log_level_set(UARTAG, ESP_LOG_INFO);
        uart_set_pin(UART_NUM_2, TXD_PIN, RXD_PIN, 18, 19);
    // We won't use a buffer for sending data.
        uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart2_queue, 0);
        uart_enable_pattern_det_intr(EX_UART_NUM, '+', PATTERN_CHR_NUM, 10000, 10, 10);
        uart_pattern_queue_reset(EX_UART_NUM, 20);

    }
