#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "esp_system.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "driver/gpio.h"
#include "driver/ledc.h"

#define BUZZER 19
#define GPIO_OUTPUT_SPEED LEDC_HIGH_SPEED_MODE

#define c 261
#define d 294
#define e 329
#define f 349
#define g 391
#define gS 415
#define a 440
#define aS 455
#define b 466
#define cH 523
#define cSH 554
#define dH 587
#define dSH 622
#define eH 659
#define fH 698
#define fSH 740
#define gH 784
#define gSH 830
#define aH 880

void sound(uint32_t freq,uint32_t duration) {

	ledc_timer_config_t timer_conf;
	timer_conf.speed_mode = GPIO_OUTPUT_SPEED;
	timer_conf.bit_num    = LEDC_TIMER_10_BIT;
	timer_conf.timer_num  = LEDC_TIMER_0;
	timer_conf.freq_hz    = freq;
	ledc_timer_config(&timer_conf);

	ledc_channel_config_t ledc_conf;
	ledc_conf.gpio_num   = BUZZER;
	ledc_conf.speed_mode = GPIO_OUTPUT_SPEED;
	ledc_conf.channel    = LEDC_CHANNEL_0;
	ledc_conf.intr_type  = LEDC_INTR_DISABLE;
	ledc_conf.timer_sel  = LEDC_TIMER_0;
	ledc_conf.duty       = 0x0; // 50%=0x3FFF, 100%=0x7FFF for 15 Bit
	                            // 50%=0x01FF, 100%=0x03FF for 10 Bit
	ledc_channel_config(&ledc_conf);

	// start
    ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0, 0x7F); // 12% duty - play here for your speaker or buzzer
    ledc_update_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0);
	vTaskDelay(duration/portTICK_PERIOD_MS);
	// stop
    ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0, 0);
    ledc_update_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0);

}

void open_sound() {
	sound(660,10);
	vTaskDelay(10/portTICK_PERIOD_MS);
	sound(660,10);
	vTaskDelay(30/portTICK_PERIOD_MS);
	sound(660,10);
	vTaskDelay(30/portTICK_PERIOD_MS);
	sound(510,10);
	vTaskDelay(10/portTICK_PERIOD_MS);
	sound(660,10);
	vTaskDelay(30/portTICK_PERIOD_MS);
	sound(770,10);
	vTaskDelay(55/portTICK_PERIOD_MS);
	sound(380,10);
	vTaskDelay(57/portTICK_PERIOD_MS);
/*
	sound(510,100);
	vTaskDelay(450/portTICK_PERIOD_MS);
	sound(380,100);
	vTaskDelay(400/portTICK_PERIOD_MS);
	sound(320,100);
	vTaskDelay(500/portTICK_PERIOD_MS);
	sound(440,100);
	vTaskDelay(300/portTICK_PERIOD_MS);
	sound(480,80);
	vTaskDelay(330/portTICK_PERIOD_MS);
	sound(450,100);
	vTaskDelay(150/portTICK_PERIOD_MS);
	sound(430,100);
	vTaskDelay(300/portTICK_PERIOD_MS);
	sound(380,100);
	vTaskDelay(200/portTICK_PERIOD_MS);
	sound(660,80);
	vTaskDelay(200/portTICK_PERIOD_MS);
	sound(760,50);
	vTaskDelay(150/portTICK_PERIOD_MS);
	sound(860,100);
	vTaskDelay(300/portTICK_PERIOD_MS);
	sound(700,80);
	vTaskDelay(150/portTICK_PERIOD_MS);
	sound(760,50);
	vTaskDelay(350/portTICK_PERIOD_MS);
	sound(660,80);
	vTaskDelay(300/portTICK_PERIOD_MS);
	sound(520,80);
	vTaskDelay(150/portTICK_PERIOD_MS);
	sound(580,80);
	vTaskDelay(150/portTICK_PERIOD_MS);
	sound(480,80);
	vTaskDelay(500/portTICK_PERIOD_MS);
	*/
}
